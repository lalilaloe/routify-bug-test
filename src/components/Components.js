import UserEdit from './UserEdit.svelte'
import UserSelect from './UserSelect.svelte'

const components = {
    UserEdit,
    UserSelect
}

export default components